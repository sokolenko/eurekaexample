package com.example

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import feign.Feign
import feign.Logger
import feign.jackson.JacksonDecoder
import feign.jackson.JacksonEncoder
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.client.discovery.EnableDiscoveryClient
import org.springframework.cloud.openfeign.EnableFeignClients
import org.springframework.cloud.openfeign.support.SpringMvcContract
import org.springframework.context.annotation.Bean
import org.springframework.web.client.RestTemplate

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
class PojoServiceApplication : CommandLineRunner {

    @Bean
    fun getRestTemplate(): RestTemplate {
        return RestTemplate()
    }

    override fun run(vararg args: String?) {
        println("PojoService run: ${args.forEach { println(it) }}")
    }

    /*@Value("\${feign.userclient.url}")
    lateinit var userClientUrl: String

    @Bean
    fun getUserClient(): UserClient {
        val mapper = ObjectMapper().registerModule(KotlinModule())
        val encoder = JacksonEncoder(mapper)
        val decoder = JacksonDecoder(mapper)

        val userClient = Feign.builder()
                .contract(SpringMvcContract())
                .encoder(encoder)
                .decoder(decoder)
                .logger(Logger.ErrorLogger())
                .logLevel(Logger.Level.FULL)
                .target(UserClient::class.java, userClientUrl)
        return userClient
    }*/

}

fun main(args: Array<String>) {
    runApplication<PojoServiceApplication>(*args)
}