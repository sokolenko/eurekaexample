package com.example

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class UserController {

    @Autowired
    lateinit var userClient: UserClient

    @RequestMapping("/create")
    @ResponseBody
    fun create(): String {
        var user = userClient.getUser(1L)
        user.id = 10L
        user = userClient.createUser(user)

        return String.format("Created a user with id %d", user.id)
    }

    @RequestMapping("/read")
    internal fun read(): String {
        val users = userClient.users
        return String.format("Retrieved %d users total", users.size)
    }

    @RequestMapping("/update")
    fun update(): String {
        val user = userClient.getUser(1L)
        val oldName = user.name
        user.name = "John"
        userClient.updateUser(1L, user)

        return String.format("Update successful. User id: %d, old name: %s, new name: %s",
                user.id, oldName, user.name)
    }

    @RequestMapping("/delete")
    @ResponseBody
    fun delete(): String {
        userClient.deleteUser(1L)
        return "Deleted"
    }

}