package com.example

import com.yurasik.model.User
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestMapping

@FeignClient(name = "users", url = "\${feign.userclient.url}")
interface UserClient {

    @get:RequestMapping(method = [RequestMethod.GET], value = ["/users"])
    val users: List<User>

    @RequestMapping(method = [RequestMethod.POST], value = ["/users"])
    fun createUser(user: User): User

    @RequestMapping(method = [RequestMethod.GET], value = ["/users/{userId}"])
    fun getUser(@PathVariable("userId") userId: Long?): User

    @RequestMapping(method = [RequestMethod.PUT], value = ["/users/{userId}"], consumes = ["application/json"])
    fun updateUser(@PathVariable("userId") userId: Long?, user: User): User

    @RequestMapping(method = [RequestMethod.DELETE], value = ["/users/{userId}"])
    fun deleteUser(@PathVariable("userId") userId: Long?)
}