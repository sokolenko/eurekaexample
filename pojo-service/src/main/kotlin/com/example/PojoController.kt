package com.example

import org.springframework.core.ParameterizedTypeReference
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.client.RestTemplate
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpMethod
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController


@RestController
class PojoController {

    @Autowired
    lateinit var restTemplate: RestTemplate

    /*@Bean
    fun alwaysSampler(): AlwaysSampler {
        return AlwaysSampler()
    }*/

    @RequestMapping
    fun zipkinService1(): String {
        //LOG.info("Inside zipkinService 1..")

        return "Hi..."
    }

    /*companion object {
        private val LOG = Logger.(ZipkinController::class.java.name)
    }*/
}