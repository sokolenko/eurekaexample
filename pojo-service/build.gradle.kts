
plugins {
    kotlin("jvm")
}

dependencies {
    (kotlin("stdlib-jdk8"))
    compile("org.springframework.boot:spring-boot-starter-actuator")
    compile("org.springframework.boot:spring-boot-starter-web")
    compile("org.springframework.boot:spring-boot-starter-parent:2.1.9.RELEASE")
    compile("org.springframework.cloud:spring-cloud-starter-netflix-eureka-client:2.1.3.RELEASE")
    compile("org.springframework.cloud:spring-cloud-starter-openfeign:2.1.3.RELEASE")
    compile("io.github.openfeign:feign-jackson:10.5.1")
    compile("com.fasterxml.jackson.module:jackson-module-kotlin:2.9.+")

    compile(project(":domain"))

    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.junit.jupiter:junit-jupiter-api")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")

    implementation(kotlin("stdlib-jdk8"))
}
