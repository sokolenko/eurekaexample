package com.example

import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.web.client.RestTemplate
import zipkin2.server.internal.EnableZipkinServer


@SpringBootApplication
@EnableZipkinServer
class ZipkinServiceApplication : CommandLineRunner {

    @Bean
    fun getRestTemplate(): RestTemplate {
        return RestTemplate()
    }

    override fun run(vararg args: String?) {
        println("ZipkinService run: ${args?.forEach { println(it) }}")
    }

}

fun main(args: Array<String>) {
    runApplication<ZipkinServiceApplication>(*args)
}