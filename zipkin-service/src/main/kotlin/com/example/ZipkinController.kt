package com.example

import org.springframework.core.ParameterizedTypeReference
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.client.RestTemplate
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpMethod
import org.springframework.web.bind.annotation.RestController


@RestController
class ZipkinController {

    @Autowired
    lateinit var restTemplate: RestTemplate

    /*@Bean
    fun alwaysSampler(): AlwaysSampler {
        return AlwaysSampler()
    }*/

    @GetMapping(value = ["/zipkin"])
    fun zipkinService1(): String {
        //LOG.info("Inside zipkinService 1..")

        val response = restTemplate!!.exchange("http://localhost:8082/zipkin2",
                HttpMethod.GET, null, object : ParameterizedTypeReference<String>() {

        }).body
        return "Hi..."
    }

    /*companion object {
        private val LOG = Logger.(ZipkinController::class.java.name)
    }*/
}