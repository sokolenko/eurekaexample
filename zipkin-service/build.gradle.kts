
plugins {
    kotlin("jvm")
}

dependencies {
    (kotlin("stdlib-jdk8"))
   /* compile("io.zipkin.java:zipkin-server:2.12.9")
    compile("io.zipkin.java:zipkin-autoconfigure-ui:2.12.9")*/
    compile("org.springframework.boot:spring-boot-starter-web"){
        exclude("org.slf4j", "slf4j-nop")
        exclude("org.slf4j", "slf4j-log4j12")
        exclude("log4j")
    }
    compile("org.springframework.boot:spring-boot-starter-parent:2.1.9.RELEASE")
    compile("org.springframework.cloud:spring-cloud-starter-netflix-eureka-client:2.1.3.RELEASE")

    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.junit.jupiter:junit-jupiter-api")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")

    implementation(kotlin("stdlib-jdk8"))
}
