
plugins {
    kotlin("jvm")
}

dependencies {
    (kotlin("stdlib-jdk8"))
    compile("io.github.openfeign:feign-jackson:10.5.1")
    compile("com.fasterxml.jackson.module:jackson-module-kotlin:2.9.+")

    testImplementation("org.junit.jupiter:junit-jupiter-api")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")
    implementation(kotlin("stdlib-jdk8"))
}
