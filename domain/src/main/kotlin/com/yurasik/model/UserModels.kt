package com.yurasik.model

data class User (var id: Long? = null,
                 var name: String? = null,
                 var username: String? = null,
                 var email: String? = null,
                 var address: Address? = null,
                 var phone: String? = null,
                 var website: String? = null,
                 var company: Company? = null)

data class Address(val street: String? = null,
                   val suite: String? = null,
                   val city: String? = null,
                   val zipcode: String? = null,
                   val geo: Geo? = null)

data class Geo(val lat: String? = null,
               val lng: String? = null)

data class Company(val name: String? = null,
                   val catchPhrase: String? = null,
                   val bs: String? = null)
