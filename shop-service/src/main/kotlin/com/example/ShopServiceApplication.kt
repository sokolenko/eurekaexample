package com.example

import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.client.discovery.EnableDiscoveryClient

@EnableDiscoveryClient
@SpringBootApplication
class ShopServiceApplication : CommandLineRunner {

    override fun run(vararg args: String?) {
        println("ShopService run...")
    }

}

fun main(args: Array<String>) {
    runApplication<ShopServiceApplication>(*args)
}