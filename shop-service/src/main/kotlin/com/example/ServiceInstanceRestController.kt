package com.example

import org.springframework.cloud.client.discovery.DiscoveryClient
import com.netflix.discovery.shared.Applications
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.client.ServiceInstance
import org.springframework.web.bind.annotation.RestController

@RestController
class ServiceInstanceRestController {

    @Autowired
    lateinit var discoveryClient: DiscoveryClient

    @RequestMapping("/service-instances/{applicationName}")
    fun serviceInstancesByApplicationName(@PathVariable applicationName: String)
            : List<ServiceInstance>
    {
        return discoveryClient.getInstances(applicationName)
    }
}