package com.example

import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.client.discovery.EnableDiscoveryClient
import org.springframework.cloud.netflix.zuul.EnableZuulProxy

@EnableDiscoveryClient
@SpringBootApplication
@EnableZuulProxy
class GatewayServiceApplication : CommandLineRunner {

    override fun run(vararg args: String?) {
        println("GatewayService run: ${args.forEach { println(it) }}")
    }

}

fun main(args: Array<String>) {
    runApplication<GatewayServiceApplication>(*args)
}