package com.example

import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer

@SpringBootApplication
@EnableEurekaServer
class DiscoveryServiceApplication : CommandLineRunner {
    override fun run(vararg args: String?) {
        println("Discovery service run...")
    }

}

fun main(args: Array<String>) {
    runApplication<DiscoveryServiceApplication>(*args)
}